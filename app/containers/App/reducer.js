import { fromJS } from 'immutable';
import {
  FOO,
  SET_MEDIA,
} from './constants';

const initialState = fromJS({
  foo: '',
});

function appReducer(state = initialState, action) {
  switch (action.type) {
    case FOO:
      return state
      .set('foo', action.foo);
    default:
      return state;
  }
}

export default appReducer;
