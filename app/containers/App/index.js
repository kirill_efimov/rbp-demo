import React from 'react';
import { Switch, Route } from 'react-router-dom';
import HomePage from 'containers/HomePage/Loadable';

export default function App() {
  return (
    <div style={{ display: 'flex', flexDirection: 'column', flex: '1 0 auto', minHeight: '100vh' }}>
      <Switch>
        <Route exact path='/' component={HomePage} />
        <Route component={HomePage} />
      </Switch>
    </div>
  );
}
