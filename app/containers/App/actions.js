import {
  FOO,
} from './constants';

export function foo() {
  return {
    type: FOO,
    foo: '',
  };
}
