import { injectGlobal } from 'styled-components';

/* eslint no-unused-expressions: 0 */
injectGlobal`
  html,
  body {
    height: 100%;
    width: 100%;
  }

  body {
    font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  body.fontLoaded {
    font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  #app {
    background-color: #fafafa;
    min-height: 100%;
    min-width: 100%;
  }

  p,
  label {
    font-family: Georgia, Times, 'Times New Roman', serif;
    line-height: 1.5em;
  }

  body:before {
    content: "0";
    display: none; /* Prevent from displaying. */
  }

  /*768*/
  @media (min-width: 24em) {
    body:before {
      content: "375";
    }
  }

  /*768*/
  @media (min-width: 48em) {
    body:before {
      content: "768";
    }
  }
  /*1024*/
  @media (min-width: 64em) {
    body:before {
      content: "1024";
    }
  }
  /*1366*/
  @media (min-width: 85em) {
    body:before {
      content: "1366";
    }
  }
  /*2560*/
  @media (min-width: 160em) {
    body:before {
      content: "2560";
    }
  }

`;
