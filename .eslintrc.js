module.exports = {
    "extends": "airbnb",
    "parser": "babel-eslint",
    "globals": {
      "window": true,
      "document": true
    },
    "rules": {
      "react/jsx-filename-extension": "off",
      "arrow-parens": 0,
      "arrow-body-style": 0,
      "comma-dangle": [
        2,
        "always-multiline"
      ],
      "func-names": [
        "error",
        "never"
      ],
      "prefer-arrow-callback": 0,
      "global-require": 0,
      "import/imports-first": [
        "warn",
        "DISABLE-absolute-first"
      ],
      "import/newline-after-import": 0,
      "import/no-dynamic-require": 0,
      "import/no-extraneous-dependencies": 0,
      "import/no-named-as-default": 0,
      "import/no-unresolved": 0,
      "import/prefer-default-export": 0,
      "indent": 0,
      "jsx-a11y/aria-props": 2,
      "jsx-a11y/heading-has-content": 0,
      "jsx-a11y/href-no-hash": 0,
      "jsx-a11y/label-has-for": 2,
      "jsx-a11y/mouse-events-have-key-events": 2,
      "jsx-a11y/role-has-required-aria-props": 2,
      "jsx-a11y/role-supports-aria-props": 2,
      "max-len": 0,
      "newline-per-chained-call": 0,
      "no-confusing-arrow": 0,
      "no-console": 0,
      "no-use-before-define": 0,
      "prefer-template": 2,
      "class-methods-use-this": 0,
      "react/forbid-prop-types": 0,
      "react/jsx-first-prop-new-line": [
        2,
        "multiline"
      ],
      "react/jsx-filename-extension": 0,
      "react/jsx-no-target-blank": 0,
      "react/require-extension": 0,
      "react/self-closing-comp": 0,
      "redux-saga/no-yield-in-race": 2,
      "redux-saga/yield-effects": 2,
      "require-yield": 0,
      "import/no-webpack-loader-syntax": 0,
      "react/prefer-stateless-function": 0,
      "blocks": 0,
      "import/extensions": 0,
      "jsx-quotes": 0,
      "react/prop-types": 0,
      "no-underscore-dangle": 0,
      "jsx-a11y/no-static-element-interactions": 0,
      "react/no-unused-prop-types": 0,
      "react/no-danger": 0,
      "no-shadow": 0,
      "no-param-reassign": 0,
      "jsx-a11y/anchor-is-valid": [
        "warn",
        {
          "aspects": [
            "invalidHref"
          ]
        }
      ],
      "import/no-named-as-default-member": 0,
      "import/no-duplicates": 0,
      "function-paren-newline": 0,
      "react/no-array-index-key": 0,
      "object-curly-newline": 0,
      "no-class-assign": 0,
      "jsx-a11y/click-events-have-key-events": 0,
      "import/no-absolute-path": 0,
      "react/destructuring-assignment": 0,
      "lines-between-class-members": 0,
      "redux-saga/no-yield-in-race": 0,
      "redux-saga/yield-effects": 0,
      "implicit-arrow-linebreak": 0,
      "operator-linebreak": 0,
      "react/jsx-wrap-multilines": 0,
      "camelcase": 0
    }
};
